FileWatcher
===========

FileWatcher is a micro java library used to monitor directory changes.

Usages
------

### Print changed files names 
```java

class Example {
    public static void main(String[] args){
        File directory; // the directory to watch
        // ... 
        
        List<Consumer<File>> consumers = List.of(
                file -> System.out.println(file.getName())
        ); // the file consumers used when a change is detected
        
        DirectoryChangeWatcher watcher =
                new DirectoryChangeWatcher(
                        directory,
                        consumers
                );
        watcher.watch(); // to start the watcher
        
        // ...
        
        watcher.stop(); // to stop  the watcher.
        
        }
}
```


Project
-------

### License

TODO

### Build

The sources are written in Java 10+ and can be found in `./src/`

to build the project the command is :
 
    mvn install 


### Tests

Test are written with Junit 5 and can be found in `./src/test/java` 

To run the tests the command is : 

    mvn test