/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2018 Yegor Bugayenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.newideal.filewatcher;

import io.newideal.filewatcher.fakes.FakePathWatchEvent;
import io.newideal.filewatcher.fakes.FakeWatchService;
import io.newideal.filewatcher.fakes.InterruptedWatchService;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * Tests the features of {@link DirectoryChangesPoller}.
 *
 * @since 1.0
 */
public final class DirectoryChangesPollerTest {

    /**
     * Short time used for long Thread.sleep() in millis.
     */
    private static final long SHORT_SLEEP = 100L;

    /**
     * Check that {@link DirectoryChangesPoller} call an
     * InterruptedException handler when WatchService is interrupted.
     *
     * @throws InterruptedException if a Multithreading issue occurred.
     */
    @Test
    @Tag("Multithreading")
    @DisplayName("a handler is called when the watcher is interrupted")
    public void checkInterruptionHandler()
        throws InterruptedException {
        final WatchService watchservice = new InterruptedWatchService();
        final List<InterruptedException> exceptions = new ArrayList<>(1);
        final Future<?> future =
            Executors.newFixedThreadPool(1)
                .submit(
                    new RepeatedRunnable(
                        new DirectoryChangesPoller(
                            watchservice,
                            Collections.emptyList()
                        ),
                        e -> exceptions.add(e),
                        new AtomicBoolean(true)
                    )
                );
        Thread.sleep(DirectoryChangesPollerTest.SHORT_SLEEP);
        Assertions.assertTrue(future.isDone());
        Assertions.assertEquals(1, exceptions.size());
    }

    /**
     * Check that {@link DirectoryChangesPoller} can be stopped.
     *
     * @throws InterruptedException if a Multithreading issue occurred.
     * @throws IOException if the creation of the temp directory fails.
     */
    @Test
    @Tag("Multithreading")
    @DisplayName("the watcher is stoppable")
    public void checkPollerStops()
        throws InterruptedException, IOException {
        final Path directory = Files.createTempDirectory("folder");
        final WatchService watchservice = new FakeWatchService(
            directory,
            List.of(new FakePathWatchEvent(directory))
        );
        final List<InterruptedException> exceptions = new ArrayList<>(1);
        final AtomicBoolean running = new AtomicBoolean(true);
        final Future<?> future =
            Executors.newFixedThreadPool(1)
                .submit(
                    new RepeatedRunnable(
                        new DirectoryChangesPoller(
                            watchservice,
                            Collections.emptyList()
                        ),
                        e -> exceptions.add(e),
                        running
                    )
                );
        running.set(false);
        Thread.sleep(DirectoryChangesPollerTest.SHORT_SLEEP);
        Assertions.assertTrue(future.isDone());
        Assertions.assertTrue(exceptions.isEmpty());
    }

}
