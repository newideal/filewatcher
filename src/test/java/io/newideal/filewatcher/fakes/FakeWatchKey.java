/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2018 Yegor Bugayenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.newideal.filewatcher.fakes;

import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.Watchable;
import java.util.List;

/**
 * A Fake WatchKey.
 * Encapsulate a Watchable and a list of WatchEvent. Used for test.
 * @since 1.0
 */
public final class FakeWatchKey implements WatchKey {

    /**
     * A path.
     */
    private final Path path;

    /**
     * A list of WatchEvent.
     */
    private final List<WatchEvent<?>> events;

    /**
     * Constructor.
     * @param path The encapsulated path.
     * @param events The encapsulated list of WatchEvent.
     */
    FakeWatchKey(final Path path, final List<WatchEvent<?>> events) {
        this.path = path;
        this.events = events;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public List<WatchEvent<?>> pollEvents() {
        return this.events;
    }

    @Override
    public boolean reset() {
        return true;
    }

    @Override
    public void cancel() {
        //Do nothing.
    }

    @Override
    public Watchable watchable() {
        return this.path;
    }
}
