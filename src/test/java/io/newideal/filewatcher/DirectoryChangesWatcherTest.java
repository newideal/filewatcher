/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2018 Yegor Bugayenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.newideal.filewatcher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * Tests the features of {@link DirectoryChangesWatcher}.
 *
 * @since 1.0
 */
public final class DirectoryChangesWatcherTest {

    /**
     * Long time used for long Thread.sleep() in millis.
     */
    private static final long LONG_SLEEP = 3000L;

    /**
     * Check that consumers are called when a file is added to a watched folder.
     * A temp directory is created,
     * then the DirectoryChangesWatcher with a consumer
     * that add to a list the file it receive.
     * Create a temporary file in the temp directory.
     * Finally assert that the list of received files contains only the file.
     *
     * @throws IOException if the directory or the file can't be created.
     * @throws InterruptedException if a Multithreading issue occurred.
     */
    @Test
    @Tag("Slow")
    @Tag("Multithreading")
    @DisplayName("consumers are called when file is added to a watched folder")
    public void checkAddition()
        throws IOException, InterruptedException {
        final Path directory = Files.createTempDirectory("fileAdded");
        final CountDownLatch latch = new CountDownLatch(1);
        final List<String> files = new ArrayList<>(1);
        final DirectoryChangesWatcher watcher =
            new DirectoryChangesWatcher(
                directory.toFile(),
                List.of(
                    new OnCreationFileConsumer(f -> files.add(f.getName())),
                    new OnCreationFileConsumer(f -> latch.countDown())
                )
            );
        watcher.watch();
        final String file =
            Files.createTempFile(directory, "", "_new-file.txt")
                .getFileName()
                .toString();
        latch.await();
        watcher.stop();
        Assertions.assertEquals(List.of(file), files);
    }

    /**
     * Check that consumers are called when a file is deleted in a folder.
     * A temp directory is created with a file,
     * then the DirectoryChangesWatcher with a consumer
     * that add to a list the file it receive.
     * Delete the file in the temp directory.
     * Finally assert that the received files contains the deleted file.
     *
     * @throws IOException
     *  if the directory or the file can't be created or deleted.
     * @throws InterruptedException if a Multithreading issue occurred.
     */
    @Test
    @Tag("Slow")
    @Tag("Multithreading")
    @DisplayName("consumers are called when a file is deleted in a folder")
    public void checkDeletion()
        throws IOException, InterruptedException {
        final Path directory = Files.createTempDirectory("fileDeleted");
        final CountDownLatch latch = new CountDownLatch(1);
        final List<String> files = new ArrayList<>(1);
        final DirectoryChangesWatcher watcher =
            new DirectoryChangesWatcher(
                directory.toFile(),
                List.of(
                    new OnDeletionFileConsumer(f -> files.add(f.getName())),
                    new OnCreationFileConsumer(f -> latch.countDown())
                )
            );
        watcher.watch();
        final Path tempfilepath =
            Files.createTempFile(directory, "", "_new-file.jpg");
        latch.await();
        tempfilepath.toFile().delete();
        Thread.sleep(DirectoryChangesWatcherTest.LONG_SLEEP);
        watcher.stop();
        Assertions.assertEquals(
            List.of(
                tempfilepath
                    .getFileName()
                    .toString()
            ),
            files
        );
    }

    /**
     * Check that consumers are called when a file is updated in a folder.
     * A temp directory is created with a file,
     * then the DirectoryChangesWatcher with a consumer
     * that add to a list the file it receive.
     * Update the file in the temp directory.
     * Finally assert that the received files contains the updated file.
     *
     * @throws IOException
     *  if the directory or the file can't be created or updated.
     * @throws InterruptedException if a Multithreading issue occurred.
     */
    @Test
    @Tag("Slow")
    @Tag("Multithreading")
    @DisplayName("consumers are called when a file is changed in a folder")
    public void checkModification()
        throws IOException, InterruptedException {
        final Path directory = Files.createTempDirectory("fileChanged");
        final CountDownLatch latch = new CountDownLatch(1);
        final List<String> files = new ArrayList<>(1);
        final DirectoryChangesWatcher watcher =
            new DirectoryChangesWatcher(
                directory.toFile(),
                List.of(
                    new OnModificationFileConsumer(f -> files.add(f.getName())),
                    new OnCreationFileConsumer(f -> latch.countDown())
                )
            );
        watcher.watch();
        final Path tempfilepath =
            Files.createTempFile(directory, "", "_update-file.jpg");
        latch.await();
        Files.write(tempfilepath, "content change".getBytes());
        Thread.sleep(DirectoryChangesWatcherTest.LONG_SLEEP);
        watcher.stop();
        Assertions.assertEquals(
            List.of(
                tempfilepath
                    .getFileName()
                    .toString()
            ),
            files
        );
    }
}
