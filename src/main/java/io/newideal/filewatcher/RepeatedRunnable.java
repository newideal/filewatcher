/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2018 Yegor Bugayenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.newideal.filewatcher;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

/**
 * Continuously call the encapsulated operation until stopped or interrupted.
 *
 * @since 1.0
 */
public final class RepeatedRunnable implements Runnable {

    /**
     * The DirectoryChangesPoller to repeat.
     */
    private final DirectoryChangesPoller poller;

    /**
     * The running state, will stop if false.
     */
    private final AtomicBoolean running;

    /**
     * The InterruptedException handler, used if the thread is interrupted.
     */
    private final Consumer<InterruptedException> handler;

    /**
     * Primary Constructor.
     *
     * @param poller The poller to repeat.
     * @param handler The interruption handler.
     * @param running The share running state.
     */
    RepeatedRunnable(
        final DirectoryChangesPoller poller,
        final Consumer<InterruptedException> handler,
        final AtomicBoolean running
    ) {
        this.poller = poller;
        this.running = running;
        this.handler = handler;
    }

    /**
     * Secondary Constructor.
     *
     * @param poller The poller to repeat.
     * @param running The share running state.
     */
    RepeatedRunnable(
        final DirectoryChangesPoller poller,
        final AtomicBoolean running
    ) {
        this(poller, Throwable::printStackTrace, running);
    }

    @Override
    public void run() {
        while (this.running.get()) {
            try {
                this.poller.pollFileEvents();
            } catch (final InterruptedException exception) {
                this.handler.accept(exception);
                throw new IllegalStateException(exception);
            }
        }
    }
}
