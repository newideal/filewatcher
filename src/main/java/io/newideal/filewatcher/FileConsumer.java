/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2018 Yegor Bugayenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.newideal.filewatcher;

import java.io.File;
import java.nio.file.WatchEvent.Kind;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * A lambda file consumer wrapper, that apply only to specifics WatchEvents.
 *
 * @since 1.0
 */
@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
abstract class FileConsumer {

    /**
     * The wrapped file consumer.
     */
    private final Consumer<File> consumer;

    /**
     * The list of kinds to which this consumer apply.
     */
    private final List<Kind> kinds;

    /**
     * Primary Constructor.
     *
     * @param consumer The wrapper lambda consumer.
     * @param kinds The kinds of events that apply to this consumer.
     */
    FileConsumer(final Consumer<File> consumer, final Kind... kinds) {
        this.consumer = consumer;
        this.kinds = Arrays.asList(kinds);
    }

    /**
     * Performs the wrapped consumer operation on the given file,
     * if the given kind matches.
     *
     * @param file The file to be consumed.
     * @param kind The kind of the WatchEvent.
     */
    @SuppressWarnings("PMD.DefaultPackage")
    final void accept(final File file, final Kind kind) {
        if (this.kinds.contains(kind)) {
            this.consumer.accept(file);
        }
    }

}

