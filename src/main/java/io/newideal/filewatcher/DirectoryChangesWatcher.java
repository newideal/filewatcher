/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2018 Yegor Bugayenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.newideal.filewatcher;

import com.sun.nio.file.SensitivityWatchEventModifier;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchService;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Trigger file consumer when a file system change in the directory happen.
 *
 * @since 1.0
 */
public final class DirectoryChangesWatcher {

    /**
     * The directory to watch.
     */
    private final File directory;

    /**
     * The consumers to use on changes.
     */
    private final List<FileConsumer> consumers;

    /**
     * The running status of the watcher.
     * If false the {@link DirectoryChangesPoller} stops.
     */
    private final AtomicBoolean running;

    /**
     * Constructor.
     *
     * @param directory The directory to watch.
     * @param consumers The consumers to triggers.
     */
    public DirectoryChangesWatcher(
        final File directory,
        final List<FileConsumer> consumers
    ) {
        this.directory = directory;
        this.consumers = consumers;
        this.running = new AtomicBoolean(false);
    }

    /**
     * Start to watch the directory.
     * In order to improve performances on some OS,
     * SensitivityWatchEventModifier.HIGH is used.
     *
     * @throws IOException if the creation of a {@link WatchService} fails.
     */
    public void watch() throws IOException {
        final WatchService service = FileSystems.getDefault().newWatchService();
        this.directory.toPath().register(
            service,
            new Kind[]{
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE,
                StandardWatchEventKinds.ENTRY_MODIFY,
                StandardWatchEventKinds.OVERFLOW,
            },
            SensitivityWatchEventModifier.HIGH
        );
        this.running.set(true);
        Executors.newFixedThreadPool(1)
            .submit(
                new RepeatedRunnable(
                    new DirectoryChangesPoller(
                        service,
                        this.consumers
                    ),
                    this.running
                )
            );
    }

    /**
     * Stop to watch the directory.
     */
    public void stop() {
        this.running.set(false);
    }

}
