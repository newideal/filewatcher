/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017-2018 Yegor Bugayenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.newideal.filewatcher;

import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

/**
 * Poll {@link WatchService} events and call suitable file consumer.
 *
 * @since 1.0
 */
final class DirectoryChangesPoller {

    /**
     * The wrapped {@link WatchService}.
     */
    private final WatchService service;

    /**
     * The consumers to trigger.
     */
    private final List<FileConsumer> consumers;

    /**
     * Primary Constructor.
     *
     * @param service The wrapped WatchService.
     * @param consumers The file consumers to use on changes.
     */
    DirectoryChangesPoller(
        final WatchService service,
        final List<FileConsumer> consumers
    ) {
        this.service = service;
        this.consumers = consumers;
    }

    /**
     * Poll the events from the {@link WatchService} and consume each of them.
     *
     * @throws InterruptedException
     *  If {@link WatchService} is interrupted while waiting.
     */
    @SuppressWarnings("PMD.DefaultPackage")
    void pollFileEvents() throws InterruptedException {
        final WatchKey key = this.service.take();
        for (final WatchEvent<?> event : key.pollEvents()) {
            final WatchEvent.Kind<?> kind = event.kind();
            if (kind != StandardWatchEventKinds.OVERFLOW) {
                this.consume((Path) event.context(), kind);
            }
        }
        key.reset();
    }

    /**
     * Consume the event by all the consumers.
     *
     * @param filename The path of the changed file.
     * @param kind The kind of the event.
     */
    private void consume(
        final Path filename,
        final WatchEvent.Kind<?> kind
    ) {
        for (final FileConsumer consumer : this.consumers) {
            consumer.accept(filename.toFile(), kind);
        }
    }
}
